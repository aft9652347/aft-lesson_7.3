package org.example;

public class MeasureTimeDemo {
    public static void main(String[] args) {
        long timeBefore = System.currentTimeMillis(); //запомнили время до выполнения операции
        calculateImportantNumbers(); //!!! операция, время работы которой мы оцениваем
        long timeAfter = System.currentTimeMillis(); //запомнили время после выполнения операции
        long result = timeAfter - timeBefore; //получаем разницу во времени - реальное время выполнения операции
        System.out.println(String.format("Время выполнения операции: %d миллисекунд", result));
    }
    /**
     * Тут код, производительность которого мы замеряем
     */
    public static void calculateImportantNumbers() {
        for (int i = 0; i < 1000000; i++) {
            System.out.println("Теперь i = " + i);
        }
    }
}