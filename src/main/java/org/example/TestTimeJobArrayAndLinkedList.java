package org.example;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TestTimeJobArrayAndLinkedList {
    public static void main(String[] args) {

        //Создание списка на основе массива
        ArrayList<Integer> aList = new ArrayList<>();
//        //Вывести в консоль размер списка до наполнения
//        System.out.println("Размер ArrayList до наполнения:" + aList.size());
//        //запомнили время до выполнения операции
//        long timeBeforeFillingAList = System.currentTimeMillis();
//        //Вызов метода для наполнения списка
//        fillingList(aList);
//        //запомнили время после выполнения операции
//        long timeAfterFillingAList = System.currentTimeMillis();
//        //получаем разницу во времени - реальное время выполнения операции
//        long resultTimeFillingAList = timeAfterFillingAList - timeBeforeFillingAList;
//        //Вывести размер списка после наполнения и время за которое была выполнена операция наполнения
//        System.out.println(String.format("Размер ArrayList после наполнения: %d; \n" +
//                "Время затраченное на наполнение списка: %d мс.", aList.size(), resultTimeFillingAList));

        //Вывести в консоль размер списка до наполнения
        System.out.println("Размер ArrayList до наполнения:" + aList.size());
        //запомнили время до выполнения операции
        beforeTimeMethod();
        //Вызов метода для наполнения списка
        fillingList(aList);
        //Запомнить время после выполнения операции, вывести размер списка после наполнения и время за которое была выполнена операция наполнения
        System.out.println(String.format("Размер ArrayList после наполнения: %d; \n" +
                "Время затраченное на наполнение списка: %d мс.", aList.size(), operationExecutionTime(beforeTime)));

        //запомнили время до выполнения операции чтения и печати
        beforeTimeMethod();
        //Вызов метода для печати из середины списка ArrayList
        printingValueTheMiddleOfList(aList);
        ////Запомнить время после выполнения операции, вывести время затраченное на чтение данных из середины списка
        System.out.println(String.format("Время затраченное на чтение данных из середины списка ArrayList: %d мс.", operationExecutionTime(beforeTime)));

        //запомнили время до выполнения операции удаления
        beforeTimeMethod();
        //Вызов метода для удаления элементов из списка ArrayList
        removeElementsOfList(aList);
        //Запомнили время после выполнения операции, вывели время затраченное на удаление элементов
        System.out.println(String.format("Время затраченное на удаление элементов из списка ArrayList: %d мс.", operationExecutionTime(beforeTime)));

        //запомнили время до выполнения операции изменения элементов в списке
        beforeTimeMethod();
        //Вызов метода для удаления элементов из списка ArrayList
        changeElementsOfList(aList);
        //Запомнили время после выполнения операции, вывели время за которое была выполнена операция изменения
        System.out.println(String.format("Время затраченное на изменение элементов в списке ArrayList: %d мс. \n", operationExecutionTime(beforeTime)));

//        //запомнили время до выполнения операции удаления
//        beforeTime();
//        //Вызов метода для удаления элементов из списка ArrayList
//        removeElementsOfList(aList);
//        //запомнили время после выполнения операции
//        timeResult(beforeTime());
//        //Вывести размер списка после наполнения и время за которое была выполнена операция наполнения
//        System.out.println(String.format("Время затраченное на удаление элементов из списка ArrayList: %d мс. \n", timeResult(beforeTime())));

        //Создание списка на основе массива
        LinkedList<Integer> lList = new LinkedList<>();
        //Вывести в консоль размер списка до наполнения
        System.out.println("Размер LinkedList до наполнения:" + lList.size());
        //запомнили время до выполнения операции
        beforeTimeMethod();
        //Вызов метода для наполнения списка
        fillingList(lList);
        //Запомнили время после выполнения операции, вывели размер списка после наполнения и затраченное на наполнение время
        System.out.println(String.format("Размер LinkedList после наполнения: %d; \n" +
                "Время затраченное на наполнение списка LinkedList: %d мс.", lList.size(), operationExecutionTime(beforeTime)));

        //запомнили время до выполнения операции чтения
        beforeTimeMethod();
        //Вызов метода для вывода на печать элементов из центра
        printingValueTheMiddleOfList(lList);
        //запомнили время после выполнения операции, вывели затраченное время
        System.out.println(String.format("Время затраченное на извлечение элементов из середины списка LinkedList: %d мс.", operationExecutionTime(beforeTime)));

        //запомнили время до выполнения операции удаления
        beforeTimeMethod();
        //Вызов метода для удаления элементов из списка
        removeElementsOfList(lList);
        //запомнили время после выполнения операции и вывели затраченное время
        System.out.println(String.format("Время затраченное на удаление элементов из списка LinkedList: %d мс.", operationExecutionTime(beforeTime)));

        //запомнили время до выполнения операции удаления
        beforeTimeMethod();
        //Вызов метода для удаления элементов из списка
        changeElementsOfList(lList);
        //Вывести размер списка после наполнения и затраченное на наполнение время
        System.out.println(String.format("Время затраченное на изменение элементов из списка LinkedList: %d мс.", operationExecutionTime(beforeTime)));

//        //запомнили время до выполнения операции удаления
//        beforeTimeMethod();
//        //Вызов метода для удаления элементов из списка
//        changeElementsOfList(lList);
//        //запомнили время после выполнения операции
//        afterTimeMethod();
//        //Вывести размер списка после наполнения и затраченное на наполнение время
//        timeResult(afterTime, beforeTime);
//        System.out.println(String.format("Время затраченное на изменение элементов из списка LinkedList: %d мс.\n\n\n\n", resultTime));

//
//        //запомнили время до выполнения операции удаления
//        long timeBeforeChangeLList = System.currentTimeMillis();
//        //Вызов метода для удаления элементов из списка
//        changeElementsOfList(lList);
//        //запомнили время после выполнения операции
//        long timeAfterChangeLList = System.currentTimeMillis();
//        //получаем разницу во времени - реальное время выполнения операции
//        long resultTimeChangeLList = timeAfterChangeLList - timeBeforeChangeLList;
//        //Вывести размер списка после наполнения и затраченное на наполнение время
//        System.out.println(String.format("Время затраченное на изменение элементов из списка LinkedList: %d мс.", resultTimeChangeLList));
    }

    //Метод основанный на цикл для наполнения списков
    public static void fillingList(List listToFillIn) {
        for (int i = 0; i < 1000000; i++) {
            listToFillIn.add(i);
        }
        System.out.println(String.format("Список наполнен %d элементов ", listToFillIn.size()));
    }

    //Метод для печати 100 элементов из середины
    public static void printingValueTheMiddleOfList(List readingList) {
        for (int i = 500000; i < 500100; i++) {
            System.out.println(readingList.get(i));
        }
    }

    //Метод для удаления 1000 первых элементов из списка
    public static void removeElementsOfList(List listForDeletingItems) {
        for (int i = 0; i < 1000; i++) {
            listForDeletingItems.remove(i);
            //System.out.println(listForDeletingItems.get(i));
        }
    }

    //метод для присвоения нового значения 100 элементов из середины
    public static void changeElementsOfList(List listForChangeItems) {
        for (int i = 0; i < 1000; i++) {
            listForChangeItems.set(i, 999999999 + i * 8);
            //System.out.println(listForChangeItems.get(i));
        }
    }

    public static long beforeTime;
    public static long afterTime;
    public static long resultTime;

    public static long beforeTimeMethod() {
//            System.out.println("beforeTimeMethod" + System.currentTimeMillis());
        return beforeTime = System.currentTimeMillis();
    }

    public static long afterTimeMethod() {
        System.out.println("afterTime" + System.currentTimeMillis());
        return afterTime = System.currentTimeMillis();
    }

    //Метод для вычисления разницы во времени
    public static long timeResult(long afterTime, long beforeTime) {
//            System.out.println("timeResult afterTime:" + afterTime);
//            System.out.println("timeResult beforeTime:" + beforeTime);
//            System.out.println(afterTime - beforeTime);
        return resultTime = TestTimeJobArrayAndLinkedList.afterTime - TestTimeJobArrayAndLinkedList.beforeTime;
    }

    //Метод для вычисления разницы во времени сокращает код
    public static long operationExecutionTime(long beforeTime) {
        //System.out.println("Б2" + beforeTime);
        //System.out.println(afterTime - beforeTime);
        afterTime = System.currentTimeMillis();
        //System.out.println("А2" + afterTime);
        return resultTime = afterTime - beforeTime;
    }
}

/**
 * Время затраченное на наполнение списка: 45 мс.
 * Время затраченное на чтение данных из середины списка ArrayList: 1 мс.
 * Время затраченное на удаление элементов из списка ArrayList: 288 мс.
 * Время затраченное на изменение элементов в списке ArrayList: 1 мс.
 *
 * Время затраченное на наполнение списка LinkedList: 107 мс.
 * Время затраченное на извлечение элементов из середины списка LinkedList: 249 мс.
 * Время затраченное на удаление элементов из списка LinkedList: 1 мс.
 * Время затраченное на изменение элементов из списка LinkedList: 1 мс.
 *
 * Делая выводы на примере работы кода можно сказать, что запись новых данных, чтение существующих
 * из середины списка быстрее осуществляется в ArrayList. Удаление элементов быстрее выполняется в списках ссылочного типа LinkedList.
 * Изменение элементов в обоих случая выполнилось быстро. Разная скорость работы на разных типах операций связана с устройством каждого из списков.
 * ArrayList список на базе массива, обращается к элементам по индексу, что позволяется быстро перемещаться и осуществлять поиск внутри списка.
 * Правда это накладывает свои ограничения в случае нехватки места, что приводит к необходимости создания нового массива (в два раза больше предыдущего)
 * и перезаписи всех элементов что замедлятся время работы. LinkedList напротив может увеличиваться с наименьшими затратами ресурсов, т.к. в его
 * основе лежит связь объектов по принципу цепочки, каждый объект имеет ссылки на предыдущий и следующий, что позволяется перезаписывать и добавлять элементы
 * более дёшево с точки зрения ресурсозатрат.
 *
 *
 *
* **/